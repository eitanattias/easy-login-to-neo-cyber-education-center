document.addEventListener("DOMContentLoaded", function () {

    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        let currentTab = tabs[0];
        let tabUrl = currentTab.url;

        if(tabUrl.split('/')[2] !== "ilearn.cyber.org.il") {
            let views = chrome.extension.getViews({type: 'popup'});

            if (views.length > 0) {
                views[0].close();
            }
        }

        console.log(tabUrl)
    });

    chrome.storage.local.get(["username", "password"], function (result) {

        const storedUsername = result.username;
        const storedPassword = result.password;

        const userIdInput = document.getElementById('PopupUsername');

        if (storedUsername) userIdInput.value = storedUsername;

        const passwordInput = document.getElementById('PopupPassword');
        if (storedPassword) passwordInput.value = storedPassword;

    });

    const usernameInput = document.getElementById("PopupUsername");
    const passwordInput = document.getElementById("PopupPassword");
    const saveButton = document.getElementById("saveButton");

    saveButton.addEventListener("click", function () {
        const username = usernameInput.value;
        const password = passwordInput.value;

        console.log(username)
        console.log(password)

        if (username && password) {
            chrome.storage.local.set({ username, password }, function () {
                console.log("Credentials saved");
            });
        } else {
            console.error("Username and password are required");
        }
    });
});
