chrome.storage.local.get(["username", "password"], function (result) {
    const storedUsername = result.username;
    const storedPassword = result.password;

    console.log(storedUsername)
    console.log(storedPassword)

    const userIdInput = document.getElementById('userid');
    userIdInput.value = storedUsername;

    const passwordInput = document.getElementById('password');
    passwordInput.value = storedPassword;

    const submitButton = document.getElementById('submit_button');
    submitButton.click();

});